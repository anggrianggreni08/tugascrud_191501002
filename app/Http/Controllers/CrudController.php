<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //mengambil data
        $karyawan = DB::table('karyawan')->get();
        //menampilkan data di view index
        return view('crud_karyawan.index', ['karyawan' => $karyawan]);
    }
    // function create fungsinya untuk melakukan create data dengan method POST
    public function create()
    {
        return view('crud_karyawan.create');
    }

    // function untuk menyimpan data yang dimasukkan ke dalam database
    public function simpan(Request $request)
    {
        DB::table('karyawan')->insert([
            //insert data ke tabel karyawan
            'nama_karyawan' => $request->nama,
            'no_karyawan' => $request->no,
            'no_telp_karyawan' => $request->telp,
            'jabatan_karyawan' => $request->jabatan,
            'divisi_karyawan' => $request->divisi
        ]);
        return redirect('/home');
    }

    // function edit untuk parameter yang dipakek id karena edit tadi menggunakan acuan ID di web.php sama index.blade.php maka disini parameternya harus sama untuk edit
    public function edit($id)
    {
        $karyawan = DB::table('karyawan')->where('id', $id)->get();
        return view('crud_karyawan.edit', ['karyawan' => $karyawan]);
    }


    //karena diatas untuk editnya sudah selesai maka update di databasenya kalau data dengann $id yang diedit terupdate didatabase
    public function update(Request $request)
    {
        DB::table('karyawan')->where('id', $request->id)->update([
            'nama_karyawan' => $request->nama,
            'no_karyawan' => $request->no,
            'no_telp_karyawan' => $request->telp,
            'jabatan_karyawan' => $request->jabatan,
            'divisi_karyawan' => $request->divisi
        ]);
        return redirect('/home');
    }

    public function destroy($id)
    {
        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/home');
    }
}