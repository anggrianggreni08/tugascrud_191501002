<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Data Karyawan</title>
</head>
<body>

    
    <div class="container-fluid px-1 py-5 mx-auto">
    <h1 class="text-center">Data Karyawan</h1>
    <div class="container ">
    <table class="table table-dark table-striped " >
        <tr>
        <th>ID</th>
            <th class="col-md-2">Nama Karyawan</th>
            <th class="col-md-2">No Karyawan</th>
            <th class="col-md-2">No Telepon</th>
            <th class="col-md-2">Jabatan</th>
            <th class="col-md-2">Divisi</th>
            <th class="col-md-2">Action</th>
        </tr>
           
        <tr>
            @foreach ($karyawan as $row )
            <tr>
                <td class="col-md-2" >{{$row->id}}</td>
                <td class="col-md-2">{{$row->nama_karyawan}}</td>
                <td class="col-md-2">{{$row->no_karyawan}}</td>
                <td class="col-md-2">{{$row->no_telp_karyawan}}</td>
                <td class="col-md-2">{{ $row->jabatan_karyawan }}</td>
                <td class="col-md-2">{{ $row->divisi_karyawan }}</td>
                <td class="col-md-2">
                    <a href="/crud_karyawan/edit/{{$row->id}}" class="btn btn-outline-primary">Edit</a>
                    <a href="/crud_karyawan/hapus/{{ $row->id }}" class="btn btn-outline-danger">Delete</a>
                </td>
            </tr>
            @endforeach
        </tr>
      
    </table>
    <div class="row justify-content-around">
        <div class="col-4">
        <a class="btn btn-outline-primary " href="/crud_karyawan/create" role="button">Input</a>
        </div>
      
    </div>
    </div>

    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    
  </body>
</html>
